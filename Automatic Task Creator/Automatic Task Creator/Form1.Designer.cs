﻿namespace Automatic_Task_Creator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser_Main = new System.Windows.Forms.WebBrowser();
            this.label_UserStories = new System.Windows.Forms.Label();
            this.textBox_UserStoriesIDs = new System.Windows.Forms.TextBox();
            this.button_RunScript = new System.Windows.Forms.Button();
            this.label_AssignTo = new System.Windows.Forms.Label();
            this.textBox_AssignTo = new System.Windows.Forms.TextBox();
            this.textBox_URL = new System.Windows.Forms.TextBox();
            this.button_Navigate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webBrowser_Main
            // 
            this.webBrowser_Main.Location = new System.Drawing.Point(0, 26);
            this.webBrowser_Main.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser_Main.Name = "webBrowser_Main";
            this.webBrowser_Main.Size = new System.Drawing.Size(1643, 807);
            this.webBrowser_Main.TabIndex = 0;
            this.webBrowser_Main.TabStop = false;
            this.webBrowser_Main.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // label_UserStories
            // 
            this.label_UserStories.AutoSize = true;
            this.label_UserStories.Location = new System.Drawing.Point(12, 842);
            this.label_UserStories.Name = "label_UserStories";
            this.label_UserStories.Size = new System.Drawing.Size(86, 13);
            this.label_UserStories.TabIndex = 0;
            this.label_UserStories.Text = "User Stories IDs:";
            // 
            // textBox_UserStoriesIDs
            // 
            this.textBox_UserStoriesIDs.Location = new System.Drawing.Point(104, 839);
            this.textBox_UserStoriesIDs.Name = "textBox_UserStoriesIDs";
            this.textBox_UserStoriesIDs.Size = new System.Drawing.Size(148, 20);
            this.textBox_UserStoriesIDs.TabIndex = 2;
            this.textBox_UserStoriesIDs.Leave += new System.EventHandler(this.textBox_UserStoriesIDs_Leave);
            this.textBox_UserStoriesIDs.MouseHover += new System.EventHandler(this.textBox_UserStoriesIDs_MouseHover);
            // 
            // button_RunScript
            // 
            this.button_RunScript.Location = new System.Drawing.Point(258, 839);
            this.button_RunScript.Name = "button_RunScript";
            this.button_RunScript.Size = new System.Drawing.Size(73, 46);
            this.button_RunScript.TabIndex = 3;
            this.button_RunScript.Text = "Run Script!";
            this.button_RunScript.UseVisualStyleBackColor = true;
            this.button_RunScript.Click += new System.EventHandler(this.button_RunScript_Click);
            // 
            // label_AssignTo
            // 
            this.label_AssignTo.AutoSize = true;
            this.label_AssignTo.Location = new System.Drawing.Point(44, 868);
            this.label_AssignTo.Name = "label_AssignTo";
            this.label_AssignTo.Size = new System.Drawing.Size(54, 13);
            this.label_AssignTo.TabIndex = 0;
            this.label_AssignTo.Text = "Assign To";
            // 
            // textBox_AssignTo
            // 
            this.textBox_AssignTo.Location = new System.Drawing.Point(104, 865);
            this.textBox_AssignTo.Name = "textBox_AssignTo";
            this.textBox_AssignTo.Size = new System.Drawing.Size(148, 20);
            this.textBox_AssignTo.TabIndex = 3;
            this.textBox_AssignTo.MouseHover += new System.EventHandler(this.textBox_AssignTo_MouseHover);
            // 
            // textBox_URL
            // 
            this.textBox_URL.Location = new System.Drawing.Point(0, 0);
            this.textBox_URL.Name = "textBox_URL";
            this.textBox_URL.Size = new System.Drawing.Size(1574, 20);
            this.textBox_URL.TabIndex = 0;
            this.textBox_URL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_URL_KeyDown);
            // 
            // button_Navigate
            // 
            this.button_Navigate.Location = new System.Drawing.Point(1568, 0);
            this.button_Navigate.Name = "button_Navigate";
            this.button_Navigate.Size = new System.Drawing.Size(75, 20);
            this.button_Navigate.TabIndex = 1;
            this.button_Navigate.Text = "Navigate";
            this.button_Navigate.UseVisualStyleBackColor = true;
            this.button_Navigate.Click += new System.EventHandler(this.button_Navigate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1643, 901);
            this.Controls.Add(this.button_Navigate);
            this.Controls.Add(this.textBox_URL);
            this.Controls.Add(this.button_RunScript);
            this.Controls.Add(this.textBox_AssignTo);
            this.Controls.Add(this.textBox_UserStoriesIDs);
            this.Controls.Add(this.label_AssignTo);
            this.Controls.Add(this.label_UserStories);
            this.Controls.Add(this.webBrowser_Main);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser_Main;
        private System.Windows.Forms.Label label_UserStories;
        private System.Windows.Forms.TextBox textBox_UserStoriesIDs;
        private System.Windows.Forms.Button button_RunScript;
        private System.Windows.Forms.Label label_AssignTo;
        private System.Windows.Forms.TextBox textBox_AssignTo;
        private System.Windows.Forms.TextBox textBox_URL;
        private System.Windows.Forms.Button button_Navigate;
    }
}

