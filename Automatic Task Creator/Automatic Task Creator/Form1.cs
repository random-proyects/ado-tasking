﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Automatic_Task_Creator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //URL Handler
        string url = "";
        private void button_Navigate_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox_URL.Text != "")
                {
                    url = textBox_URL.Text.ToString();
                    Uri goTo = new Uri(url);
                    webBrowser_Main.Url = goTo;
                }
                else
                {
                    throw new Exception("Input an URL!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void textBox_URL_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                button_Navigate_Click(sender, e);
            }
        }

        //User Stories input handler
        List<string> idsList = new List<string>();
        string idsString;
        private void textBox_UserStoriesIDs_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.Show("Insert the work items ids, separated by commas", textBox_UserStoriesIDs, 0, 20, 3000);
        }
        private void textBox_UserStoriesIDs_Leave(object sender, EventArgs e)
        {
            idsList.Clear();
            idsString = textBox_UserStoriesIDs.Text;
            idsList = idsString.Split(',').ToList();
        }

        //Assign To input handler
        private void textBox_AssignTo_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.Show("Input a email or leave empty to assign to self", textBox_UserStoriesIDs, 0, 20, 3000);
        }

        //Run the Script
        private void button_RunScript_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < idsList.Count(); i++)
                {
                    foreach (char c in idsList[i])
                    {
                        if (!Char.IsDigit(c))
                        {
                            throw new Exception($"The id number in the position {i+1} is incorrect.");
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
